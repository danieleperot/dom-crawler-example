# DomCrawler Example

Example usage of `symfony/dom-crawler`.

It filters all html of a page and transform all the iframes and scripts attributed in order to block their execution before the user accepts GDPR regulations.