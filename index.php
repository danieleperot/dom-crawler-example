<?php

require_once 'vendor/autoload.php';

use Symfony\Component\DomCrawler\Crawler;

$html = <<<'HTML'
<!DOCTYPE html>
<html>
    <body>
        <p class="message">Hello World!</p>
        <p>Hello Crawler!</p>
        <div>
            <div>
                <div>
                    <iframe src="test" frameborder="0"></iframe>
                    <iframe src="second" frameborder="0"></iframe>
                    <iframe src="third" frameborder="0"></iframe>
                    <iframe src="fourth" frameborder="0"></iframe>
                    <script>// this is js</script>
                    <script type="text/javascript">// this is js with type</script>
                    <script src="//test.js"></script>
                    <script src="//test.js#with-type" type="text/javascript"></script>
                </div>
            </div>
        </div>
    </body>
</html>
HTML;

$crawler = new Crawler($html);

$crawler->filter('iframe')->each(function (Crawler $node, $i) {
    $node->getNode(0)->setAttribute('data-blocked', $node->attr('src'));
    $node->getNode(0)->removeAttribute('src');
    return $node;
});;

$crawler->filter('script')->each(function (Crawler $node, $i) {
    $node->getNode(0)->setAttribute('data-blocked', $node->attr('src'));
    $node->getNode(0)->removeAttribute('src');
    if ($node->attr('type')) {
        $node->getNode(0)->setAttribute('type', 'javascript/blocked');
    }
    return $node;
});;

dd($crawler->html());